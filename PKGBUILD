# This is an example PKGBUILD file. Use this as a start to creating your own,
# and remove these comments. For more information, see 'man PKGBUILD'.
# NOTE: Please fill out the license field for your package! If it is unknown,
# then please put 'unknown'.

# Maintainer: Future Linux Team <future_linux@163.com>
pkgname=perl
pkgver=5.40.0
pkgrel=1
pkgdesc="A highly capable, feature-rich programming language"
arch=('x86_64')
url="https://www.perl.org/"
license=('GPL' 'PerlArtistic')
depends=('glibc' 'libxcrypt' 'gdbm')
options=('makeflags' '!purge' 'emptydirs')
source=(https://www.cpan.org/src/5.0/${pkgname}-${pkgver}.tar.xz)
sha256sums=(d5325300ad267624cb0b7d512cfdfcd74fa7fe00c455c5b51a6bd53e5e199ef9)

build() {
	cd ${pkgname}-${pkgver}

	export BUILD_ZLIB=False
	export BUILD_BZIP2=0
	
	export TZ=UTC

	sh Configure -des                                          \
	    -Dprefix=/usr                                          \
	    -Dvendorprefix=/usr                                    \
		-Doptimize="${CFLAGS}"                                 \
		-Dlddlflags="-shared ${LDFLAGS}"                       \
		-Dldflags="${LDFLAGS}"                                 \
		-Dprivlib=/usr/lib64/perl5/${pkgver%.*}/core_perl      \
		-Darchlib=/usr/lib64/perl5/${pkgver%.*}/core_perl      \
		-Dsitelib=/usr/lib64/perl5/${pkgver%.*}/site_perl      \
		-Dsitearch=/usr/lib64/perl5/${pkgver%.*}/site_perl     \
		-Dvendorlib=/usr/lib64/perl5/${pkgver%.*}/vendor_perl  \
		-Dvendorarch=/usr/lib64/perl5/${pkgver%.*}/vendor_perl \
		-Dman1dir=/usr/share/man/man1                          \
		-Dman3dir=/usr/share/man/man3                          \
		-Dpager="/usr/bin/less -isR"                           \
		-Duseshrplib                                           \
		-Dusethreads                                           \
        -Dcc=${CHOST}-gcc

	make
}

package() {
	cd ${pkgname}-${pkgver}

	make DESTDIR=${pkgdir} install

	unset BUILD_ZLIB BUILD_BZIP2 TZ
}
